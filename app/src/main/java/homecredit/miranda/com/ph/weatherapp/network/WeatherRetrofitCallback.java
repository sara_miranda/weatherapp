package homecredit.miranda.com.ph.weatherapp.network;

import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class WeatherRetrofitCallback<K> implements Callback<K> {
    public static final String DEFAULT_ERROR_MESSAGE = "";

    @Override
    public void onResponse(Call<K> call, Response<K> response) {
        if (response.isSuccessful()) {
            isSuccess(response.body());
        } else {
            onFailure(call, new Throwable());
        }
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        if (t != null) {
            String errorMessage = DEFAULT_ERROR_MESSAGE;
            if (t instanceof UnknownHostException) {
                errorMessage = "NO INTERNET CONNECTION"; //TODO :: to String.xml
            }
            isFailed(errorMessage);
        }
    }


    public abstract void isSuccess(K data);
    public abstract void isFailed(String message);
}
