package homecredit.miranda.com.ph.weatherapp.fragments;


import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.sql.Ref;

import butterknife.BindView;
import homecredit.miranda.com.ph.weatherapp.R;
import homecredit.miranda.com.ph.weatherapp.activities.MainActivity;

public class ButtonFragment extends BaseFragment {

    @BindView(R.id.iv_refresh)          ImageView ivRefresh;

    @Override
    protected void initViews() {
        ivRefresh.setClickable(true);
        ivRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("image -- ", "refresh!!!");
                ((MainActivity)getActivity()).refresh();
            }
        });
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_button;
    }
}
