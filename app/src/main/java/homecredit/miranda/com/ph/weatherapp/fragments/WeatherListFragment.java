package homecredit.miranda.com.ph.weatherapp.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import homecredit.miranda.com.ph.weatherapp.BuildConfig;
import homecredit.miranda.com.ph.weatherapp.R;
import homecredit.miranda.com.ph.weatherapp.activities.MainActivity;
import homecredit.miranda.com.ph.weatherapp.adapters.WeatherListAdapter;
import homecredit.miranda.com.ph.weatherapp.interfaces.CustomOnItemClickListener;
import homecredit.miranda.com.ph.weatherapp.interfaces.RefreshListener;
import homecredit.miranda.com.ph.weatherapp.models.WeatherInfoResponse;
import homecredit.miranda.com.ph.weatherapp.models.WeatherInformation;
import homecredit.miranda.com.ph.weatherapp.network.WeatherClient;
import homecredit.miranda.com.ph.weatherapp.network.WeatherRetrofitCallback;

public class WeatherListFragment extends BaseFragment implements CustomOnItemClickListener {

    @BindView(R.id.rv_weather)         RecyclerView rvWeather;

    private WeatherListAdapter mWeatherListAdapter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    protected void initViews() {
        rvWeather.setLayoutManager(new LinearLayoutManager(getContext()));
        loadData();
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_weather_list;
    }

    private void loadData() {
        getWeatherList();
    }

    public void getWeatherList() {
        final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "Loading", "Loading the weather updates...");
        WeatherRetrofitCallback callback = new WeatherRetrofitCallback() {
            @Override
            public void isSuccess(Object data) {
                WeatherInfoResponse response = (WeatherInfoResponse) data;
                loadWeatherList(response.getList());
                progressDialog.dismiss();
            }
            @Override
            public void isFailed(String message) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(),"Error in retrieving weather information.", Toast.LENGTH_LONG).show();
            }
        };
        WeatherClient.getInstance().getWeatherService().getWeatherList("2643744,5391959,3067696", BuildConfig.WEATHER_API_KEY, "imperial").enqueue(callback);
    }

    private void loadWeatherList(List<WeatherInformation> weatherInformationList) {
        mWeatherListAdapter = new WeatherListAdapter(weatherInformationList, WeatherListFragment.this);
        mWeatherListAdapter.notifyDataSetChanged();
        rvWeather.setAdapter(mWeatherListAdapter);
    }

    @Override
    public void onClick(int position, Object object) {
        WeatherInformation info = (WeatherInformation) object;
        passDataToNextFrag(info.getId());
    }

    private void passDataToNextFrag(int cityId) {
        Bundle b = new Bundle();
        b.putInt("cityId", cityId);

        WeatherDetailFragment fragment = new WeatherDetailFragment();
        fragment.setArguments(b);
        goToDetailsFrag(fragment);
    }

    private void goToDetailsFrag(WeatherDetailFragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_holder, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

}
