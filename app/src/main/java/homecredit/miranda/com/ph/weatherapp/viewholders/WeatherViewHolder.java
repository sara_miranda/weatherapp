package homecredit.miranda.com.ph.weatherapp.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import homecredit.miranda.com.ph.weatherapp.R;

public class WeatherViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_location)         public TextView tvLocation;
    @BindView(R.id.tv_weather)          public TextView tvWeather;
    @BindView(R.id.tv_temp)             public TextView tvTemp;

    public WeatherViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
