package homecredit.miranda.com.ph.weatherapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import homecredit.miranda.com.ph.weatherapp.R;
import homecredit.miranda.com.ph.weatherapp.interfaces.CustomOnItemClickListener;
import homecredit.miranda.com.ph.weatherapp.models.WeatherInformation;
import homecredit.miranda.com.ph.weatherapp.viewholders.WeatherViewHolder;

public class WeatherListAdapter extends RecyclerView.Adapter<WeatherViewHolder> {

    public final CustomOnItemClickListener<WeatherInformation> customOnItemClickListener;
    protected List<WeatherInformation> weatherList;

    private Context mContext;
    private View mView;

    public WeatherListAdapter (List<WeatherInformation> weatherList, CustomOnItemClickListener customOnItemClickListener) {
        this.weatherList = weatherList;
        this.customOnItemClickListener = customOnItemClickListener;
    }

    @Override
    public WeatherViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        if (this.mContext == null) {
            mContext = parent.getContext();
        }
        mView = LayoutInflater.from(mContext).inflate(R.layout.item_weather_list, parent, false);
        return new WeatherViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(final WeatherViewHolder holder, final int position) {
        final WeatherInformation weather = weatherList.get(position);
        holder.tvLocation.setText(weather.getName());
        holder.tvWeather.setText((weather.getWeather().get(0).getDescription()));
        holder.tvTemp.setText(String.valueOf(weather.getMain().getTemp()) + " " + (char) 0x00B0 + " F");
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customOnItemClickListener.onClick(position, weather);
            }
        });
    }

    @Override
    public int getItemCount() {
        Log.d("itemcount -- ", String.valueOf(weatherList.size()));
        return weatherList.size();
    }

}
