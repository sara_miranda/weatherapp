package homecredit.miranda.com.ph.weatherapp.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;
import homecredit.miranda.com.ph.weatherapp.R;
import homecredit.miranda.com.ph.weatherapp.fragments.WeatherDetailFragment;
import homecredit.miranda.com.ph.weatherapp.fragments.WeatherListFragment;
import homecredit.miranda.com.ph.weatherapp.interfaces.RefreshListener;

public class MainActivity extends AppCompatActivity implements RefreshListener{



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        loadDefault();
    }

    private void loadDefault() {
        WeatherListFragment fragment = new WeatherListFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.fragment_holder, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void refresh() {
        onManualRefresh();
    }

    @Override
    public void onManualRefresh() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_holder);
        if (fragment instanceof WeatherListFragment) {
            ((WeatherListFragment) fragment).getWeatherList();
        } else if (fragment instanceof WeatherDetailFragment) {
            ((WeatherDetailFragment) fragment).getWeatherDetails();
        }
    }
}
