package homecredit.miranda.com.ph.weatherapp.models;

import java.util.List;

import homecredit.miranda.com.ph.weatherapp.models.WeatherInformation;

public class WeatherInfoResponse {
    private int cnt;
    private List<WeatherInformation> list;

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public List<WeatherInformation> getList() {
        return list;
    }

    public void setList(List<WeatherInformation> list) {
        this.list = list;
    }
}
