package homecredit.miranda.com.ph.weatherapp.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import homecredit.miranda.com.ph.weatherapp.BuildConfig;
import homecredit.miranda.com.ph.weatherapp.R;
import homecredit.miranda.com.ph.weatherapp.activities.MainActivity;
import homecredit.miranda.com.ph.weatherapp.interfaces.RefreshListener;
import homecredit.miranda.com.ph.weatherapp.models.WeatherInformation;
import homecredit.miranda.com.ph.weatherapp.network.WeatherClient;
import homecredit.miranda.com.ph.weatherapp.network.WeatherRetrofitCallback;

public class WeatherDetailFragment extends BaseFragment{

    @BindView(R.id.tv_loc_name)         TextView tvLocName;
    @BindView(R.id.tv_weather_desc)     TextView tvWeatherDesc;
    @BindView(R.id.tv_temperature)      TextView tvTemp;
    @BindView(R.id.tv_humidity)         TextView tvHumidity;
    @BindView(R.id.tv_wind_speed)       TextView tvWindSpeed;

    @BindView(R.id.iv_weather_icon)     ImageView ivWeatherIcon;

    private int cityId = 0;

    @Override
    protected void initViews() {
        Bundle b = getArguments();
        if (b != null) {
            cityId = b.getInt("cityId");
            getWeatherDetails(cityId);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_weather_detail;
    }

    public void getWeatherDetails() {
        getWeatherDetails(cityId);
    }

    private void getWeatherDetails(int cityId) {
        final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "Loading", "Loading the weather details...");
        WeatherRetrofitCallback callback = new WeatherRetrofitCallback() {
            @Override
            public void isSuccess(Object data) {
                WeatherInformation response = (WeatherInformation) data;
                getWeatherIcon(response.getWeather().get(0).getIcon());
                loadDetails(response);
                progressDialog.dismiss();
            }
            @Override
            public void isFailed(String message) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), "Error in retrieving weather information.", Toast.LENGTH_LONG).show();
            }
        };
        WeatherClient.getInstance().getWeatherService().getWeatherDetail(String.valueOf(cityId),
                BuildConfig.WEATHER_API_KEY,"imperial").enqueue(callback);
    }

    private void getWeatherIcon(String iconId) {
        Glide.with(WeatherDetailFragment.this)
                .load(BuildConfig.ICON_HOST + iconId + ".png")
                .into(ivWeatherIcon);
    }

    private void loadDetails(WeatherInformation weather) {
        tvLocName.setText(weather.getName());
        tvWeatherDesc.setText(weather.getWeather().get(0).getDescription());
        tvTemp.setText("TEMP " + String.valueOf(weather.getMain().getTemp()) + " " + (char) 0x00B0);
        tvHumidity.setText("HUMIDITY " + String.valueOf(weather.getMain().getHumidity()));
        tvWindSpeed.setText("WIND SPEED " + String.valueOf(weather.getWind().getSpeed()));

    }

}
