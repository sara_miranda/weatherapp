package homecredit.miranda.com.ph.weatherapp.interfaces;

public interface CustomOnItemClickListener<K> {
    void onClick(int position, K object);
}
