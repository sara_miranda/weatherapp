package homecredit.miranda.com.ph.weatherapp.network;

import homecredit.miranda.com.ph.weatherapp.models.WeatherInfoResponse;
import homecredit.miranda.com.ph.weatherapp.models.WeatherInformation;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherService {

    @GET("group?")
    Call<WeatherInfoResponse> getWeatherList(@Query("id") String id, @Query("APPID") String appId,
                                             @Query("units") String tempUnit);

    @GET("weather?")
    Call<WeatherInformation> getWeatherDetail (@Query("id") String id, @Query("APPID") String appId,
                                               @Query("units") String tempUnit);

}
