package homecredit.miranda.com.ph.weatherapp.network;

import android.util.Log;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import homecredit.miranda.com.ph.weatherapp.BuildConfig;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherClient {

    private static WeatherClient weatherClient;
    private WeatherService weatherService;

    private static final int CONNECT_TIMEOUT_MILLIS = 10000;
    private static final int READ_TIMEOUT_MILLIS =  10000;

    private WeatherClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.HOST)
                .client(getHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        weatherService = retrofit.create(WeatherService.class);
    }

    public static WeatherClient getInstance () {
        if (weatherClient == null) {
            weatherClient = new WeatherClient();
        }
        return weatherClient;
    }

    public WeatherService getWeatherService() {
        return weatherService;
    }

    private OkHttpClient getHttpClient () {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        httpClient.connectTimeout(CONNECT_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
        httpClient.readTimeout(READ_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Response response = chain.proceed(request);

                int count = 1;
                while (!response.isSuccessful() && count < 3) {
                    Log.e("intercept", "Request retry -- " + count);
                    count++;
                    response = chain.proceed(request);
                }
                return response;
            }
        });
        return httpClient.build();
    }
}
